// 1. I had some issues with flickering, changing the event to "onscrollend"
// 2. My design requires not just opacity changes but different design for active / inactive buttons so modified for this accordingly
 
// Configuration
var disabledBackground = "#aca8a9";
var disabledBorder = "#ffffff";
var enabledBackground = "#fae101";
var enabledBorder = "#ffffff";
 
var visitedBackground = "#d442a5"; 
 
PagingControl = function(scrollableView){
	
	var container = Titanium.UI.createView({
		height: Ti.UI.SIZE,
		top: "20dp",
		width: Ti.UI.SIZE
	});
	
	// Keep a global reference of the available pages
	var numberOfPages = scrollableView.getViews().length;
	var pages = []; // without this, the current page won't work on future references of the module
	
	var visitedPages= [];
	
	var previousPage = 0; //Index of previously selected paging control
	// Go through each of the current pages available in the scrollableView
	for (var i = 0; i < numberOfPages; i++) {
		var page = Titanium.UI.createView({
			borderRadius: Ti.Platform.name == 'android' ? 7 * Ti.Platform.displayCaps.logicalDensityFactor : 7 ,   //half with 
			borderWidth: '1.5dp',
			borderColor: disabledBorder,
			width: '14dp',
			height: '14dp',
			left: (22 * i) + 'dp',
			bottom: 0,
			backgroundColor: disabledBackground
		});
		
		// Store a reference to this view
		pages.push(page);
		
		// Add it to the container
		container.add(page);
	}
	
	// Mark the initial selected page
	pages[scrollableView.getCurrentPage()].setBackgroundColor(enabledBackground);
	pages[scrollableView.getCurrentPage()].setBorderColor(enabledBorder);
	
	visitedPages.push(scrollableView.getCurrentPage() );
	
	checkValue= function(a, array){
		Titanium.API.info(array);
	    for(var i=0;i<array.length;i++){
	        if(a == array[i]){
	            return true;
	        }
	    }
	    return false;
	};
	
	// Callbacks
	onScrollEnd = function(event){
		
		//Set the previously selected paging control to disabled colours 		
		pages[previousPage].setBackgroundColor(disabledBackground);
		pages[previousPage].setBorderColor(disabledBorder);
		
		if( checkValue(previousPage, visitedPages ) )
			pages[previousPage].setBackgroundColor(visitedBackground);
			
		//Set the currently selected paging control to disabled colours
		pages[event.currentPage].setBackgroundColor(enabledBackground);
		pages[event.currentPage].setBorderColor(enabledBorder);
		
		//Remember the previous page so we can use it next time onScrollEnd is called
		previousPage = event.currentPage;
		
		visitedPages.push( event.currentPage );
	};
	
	// Attach the scroll event to this scrollableView, so we know when to update things
	scrollableView.addEventListener("scrollend", onScrollEnd);
 
	return container;
};
 
module.exports = PagingControl;