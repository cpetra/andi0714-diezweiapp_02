//VARIABLES
var pWidth = 320; 
var openPage = 'home';

//INITIAL SETTINGS
$.win1.orientationModes = [Ti.UI.LANDSCAPE_LEFT, Ti.UI.LANDSCAPE_RIGHT, Ti.UI.PORTRAIT];
init();

$.win1.open();

//ADD EVENTS                         
Ti.App.addEventListener('buttonClickedEvent', handlerButtonEvent);
$.btnBack.addEventListener('click', handlerButtonBack);
$.btnDown_01.addEventListener('click', handlerClickButtonDown);
$.btnDown_02.addEventListener('click', handlerClickButtonDown);
$.btnDown_03.addEventListener('click', handlerClickButtonDown);

Ti.Gesture.addEventListener('orientationchange', handlerOrientationChange);
$.win1.addEventListener('close', handlerCloseWindow);

//FUNCTIONS 

function init() {
	pWidth = Ti.Platform.displayCaps.platformWidth;
	if(Ti.Platform.osname=="android") {
		pWidth = (Ti.Platform.displayCaps.platformWidth/Ti.Platform.displayCaps.logicalDensityFactor);	
	}
	Titanium.API.info('open page: ' + openPage);
	Titanium.API.info('width: + ' + pWidth );
	
	if(openPage == 'home') {
		$.comp.left = 0;
	} else {
		$.comp.left = -( pWidth);
	}
	
	$.contDescriptionPage.left = pWidth ;
	//$.contDescriptionPage.left = 200;
	$.contDescriptionPage.width = pWidth;
	$.home.width = pWidth;
	
	$.comp.width = 2 * pWidth;
	
}

function handlerButtonEvent(e) {
	var b = Titanium.UI.createAnimation({left: - pWidth, duration: 500});
	$.comp.animate(b);
	openPage = 'description';
	$.btnBack.visible = true;
}

function handlerButtonBack(e) {
	var b = Titanium.UI.createAnimation({left: 0, duration: 500});
	$.comp.animate(b);
	
	openPage = 'home';
	$.btnBack.visible = false;
}

function handlerClickButtonDown(e) {
	alert("click");
}

function handlerOrientationChange(e) {
	init();
	Titanium.API.info('orientation changed');
}

function handlerCloseWindow(e) {
	Ti.App.removeEventListener('buttonClickedEvent', handlerButtonEvent);
	Ti.Gesture.removeEventListener('orientationchange', handlerOrientationChange);
	$.win1.removeEventListener('close', handlerCloseWindow);
}
