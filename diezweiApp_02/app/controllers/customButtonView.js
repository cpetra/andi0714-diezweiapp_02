var args = arguments[0] || {};

function setData(p) {
	$.title.setText( p.title );
	$.txtNumber.setText( p.number );
}
function changeVisibilityArrow(p) {
	$.imgArrow.setVisible(p);
}


$.btn.addEventListener('click', function(e){
	//alert('You clicked on: ' + $.title.text);
	Ti.App.fireEvent('buttonClickedEvent');
});

exports.setData = setData;
exports.changeVisibilityArrow = changeVisibilityArrow;