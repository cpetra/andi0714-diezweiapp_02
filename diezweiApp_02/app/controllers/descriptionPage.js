var args = arguments[0] || {};

$.btn.changeVisibilityArrow(false);

$.cb.addEventListener('click', handlerClickCheckbox);

function handlerClickCheckbox(e) {
	$.imgCheck.visible ? $.imgCheck.visible = false : $.imgCheck.visible = true;  
	
}


require('PagingControl'); 
var sViewPagingControl = new PagingControl($.sview_slider); 
$.viewPaging.add(sViewPagingControl); 

if(Ti.Platform.name == 'android') {
	Ti.Gesture.addEventListener('orientationchange', function(e) {
		Titanium.API.info(Ti.Gesture.orientation);
		if(Ti.Gesture.orientation == Ti.UI.PORTRAIT){
			$.containerScroller.height = '190dp';
		} else if(Ti.Gesture.orientation == Ti.UI.LANDSCAPE_RIGHT || Ti.Gesture.orientation == Ti.UI.LANDSCAPE_LEFT) {
			$.containerScroller.height = '140dp';
		}
		
	});
}
