function Controller() {
    function setData(p) {
        $.title.setText(p.title);
        $.txtNumber.setText(p.number);
    }
    function changeVisibilityArrow(p) {
        $.imgArrow.setVisible(p);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "customButtonView";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.btn = Ti.UI.createView({
        backgroundColor: "#ffffff",
        borderColor: "#cccccc",
        borderWidth: "1.5dp",
        left: "10dp",
        right: "10dp",
        height: "40dp",
        borderRadius: "5",
        top: "5dp",
        id: "btn"
    });
    $.__views.btn && $.addTopLevelView($.__views.btn);
    $.__views.view_01 = Ti.UI.createView({
        left: "3dp",
        top: "3dp",
        right: "3dp",
        bottom: "3dp",
        backgroundColor: "#00ff00",
        id: "view_01"
    });
    $.__views.btn.add($.__views.view_01);
    $.__views.view_02 = Ti.UI.createView({
        backgroundColor: "#cccccc",
        left: "0dp",
        width: "40dp",
        id: "view_02"
    });
    $.__views.view_01.add($.__views.view_02);
    $.__views.__alloyId0 = Ti.UI.createImageView({
        image: "/images/btn-icon.png",
        id: "__alloyId0"
    });
    $.__views.view_02.add($.__views.__alloyId0);
    $.__views.txtNumber = Ti.UI.createLabel({
        font: {
            fontSize: "10dp",
            fontFamily: "Helvetica Neue",
            fontWeight: "bold"
        },
        top: "13dp",
        color: "#000000",
        text: "+3",
        id: "txtNumber"
    });
    $.__views.view_02.add($.__views.txtNumber);
    $.__views.view_03 = Ti.UI.createView({
        backgroundGradient: {
            type: "linear",
            startPoint: {
                x: "0%",
                y: "100%"
            },
            endPoint: {
                x: "0%",
                y: "0%"
            },
            colors: [ {
                color: "#e63eb5",
                offset: 0
            }, {
                color: "#fc82d7",
                offset: 1
            } ]
        },
        left: "40dp",
        right: "0dp",
        id: "view_03"
    });
    $.__views.view_01.add($.__views.view_03);
    $.__views.title = Ti.UI.createLabel({
        left: "10dp",
        font: {
            fontSize: "14dp",
            fontFamily: "Helvetica Neue",
            fontWeight: "bold"
        },
        color: "#000000",
        text: "3 Tage vor jedem Termin",
        id: "title"
    });
    $.__views.view_03.add($.__views.title);
    $.__views.__alloyId1 = Ti.UI.createView({
        id: "__alloyId1"
    });
    $.__views.view_03.add($.__views.__alloyId1);
    $.__views.imgArrow = Ti.UI.createImageView({
        right: "10dp",
        opacity: .8,
        id: "imgArrow",
        image: "/images/btn-arrow.png"
    });
    $.__views.__alloyId1.add($.__views.imgArrow);
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    $.btn.addEventListener("click", function() {
        Ti.App.fireEvent("buttonClickedEvent");
    });
    exports.setData = setData;
    exports.changeVisibilityArrow = changeVisibilityArrow;
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;