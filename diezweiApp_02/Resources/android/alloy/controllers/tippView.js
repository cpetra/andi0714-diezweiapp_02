function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "tippView";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.tippView = Ti.UI.createView({
        height: Ti.UI.SIZE,
        id: "tippView"
    });
    $.__views.tippView && $.addTopLevelView($.__views.tippView);
    $.__views.view_01 = Ti.UI.createView({
        backgroundColor: "#ffffff",
        borderColor: "#cccccc",
        borderWidth: "1.5dp",
        left: "0dp",
        right: "0dp",
        height: Ti.UI.SIZE,
        borderRadius: "5",
        top: "0dp",
        id: "view_01"
    });
    $.__views.tippView.add($.__views.view_01);
    $.__views.__alloyId42 = Ti.UI.createView({
        backgroundColor: "#8f8d8e",
        left: "3dp",
        top: "3dp",
        right: "3dp",
        height: "30dp",
        borderRadius: "2",
        layout: "horizontal",
        id: "__alloyId42"
    });
    $.__views.view_01.add($.__views.__alloyId42);
    $.__views.__alloyId43 = Ti.UI.createImageView({
        image: "/images/star.png",
        left: "5dp",
        id: "__alloyId43"
    });
    $.__views.__alloyId42.add($.__views.__alloyId43);
    $.__views.txtTipp = Ti.UI.createLabel({
        font: {
            fontSize: "18dp",
            fontFamily: "Helvetica Neue",
            fontWeight: "bold"
        },
        color: "#ffffff",
        text: "TIPP",
        id: "txtTipp",
        left: "5dp"
    });
    $.__views.__alloyId42.add($.__views.txtTipp);
    $.__views.__alloyId44 = Ti.UI.createView({
        backgroundColor: "#ffffff",
        top: "28dp",
        left: "3dp",
        right: "3dp",
        height: Ti.UI.SIZE,
        layout: "vertical",
        id: "__alloyId44"
    });
    $.__views.view_01.add($.__views.__alloyId44);
    $.__views.txt_01 = Ti.UI.createLabel({
        font: {
            fontSize: "13dp",
            fontFamily: "Helvetica Neue"
        },
        color: "#252525",
        textAlign: "left",
        top: "5dp",
        left: "10dp",
        right: "10dp",
        text: "Mit eigenen Fragen zeigst du, dass du Interesse an der Ausbildung und an der Firma hast. Du könntest zum Biespiel fragen",
        id: "txt_01"
    });
    $.__views.__alloyId44.add($.__views.txt_01);
    $.__views.__alloyId45 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        id: "__alloyId45"
    });
    $.__views.__alloyId44.add($.__views.__alloyId45);
    $.__views.__alloyId46 = Ti.UI.createImageView({
        image: "/images/icon-arrow-text.png",
        left: "10dp",
        top: "3dp",
        id: "__alloyId46"
    });
    $.__views.__alloyId45.add($.__views.__alloyId46);
    $.__views.__alloyId47 = Ti.UI.createLabel({
        font: {
            fontSize: "13dp",
            fontFamily: "Helvetica Neue"
        },
        color: "#252525",
        textAlign: "left",
        top: "0dp",
        left: "30dp",
        right: "10dp",
        text: "Wer deine Ansprechpartner in der Firma sind, wenn du dort Azubi bist",
        id: "__alloyId47"
    });
    $.__views.__alloyId45.add($.__views.__alloyId47);
    $.__views.__alloyId48 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        id: "__alloyId48"
    });
    $.__views.__alloyId44.add($.__views.__alloyId48);
    $.__views.__alloyId49 = Ti.UI.createImageView({
        image: "/images/icon-arrow-text.png",
        left: "10dp",
        top: "3dp",
        id: "__alloyId49"
    });
    $.__views.__alloyId48.add($.__views.__alloyId49);
    $.__views.__alloyId50 = Ti.UI.createLabel({
        font: {
            fontSize: "13dp",
            fontFamily: "Helvetica Neue"
        },
        color: "#252525",
        textAlign: "left",
        top: "0dp",
        left: "30dp",
        right: "10dp",
        text: "Wo genau dein Arbeitsplatz sein wird",
        id: "__alloyId50"
    });
    $.__views.__alloyId48.add($.__views.__alloyId50);
    $.__views.__alloyId51 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        id: "__alloyId51"
    });
    $.__views.__alloyId44.add($.__views.__alloyId51);
    $.__views.__alloyId52 = Ti.UI.createImageView({
        image: "/images/icon-arrow-text.png",
        left: "10dp",
        top: "3dp",
        id: "__alloyId52"
    });
    $.__views.__alloyId51.add($.__views.__alloyId52);
    $.__views.__alloyId53 = Ti.UI.createLabel({
        font: {
            fontSize: "13dp",
            fontFamily: "Helvetica Neue"
        },
        color: "#252525",
        textAlign: "left",
        top: "0dp",
        left: "30dp",
        right: "10dp",
        text: "Ob es von der Firma finanzierte Fortbildungsmöglichkeiten gibt",
        id: "__alloyId53"
    });
    $.__views.__alloyId51.add($.__views.__alloyId53);
    $.__views.__alloyId54 = Ti.UI.createView({
        height: "10dp",
        width: "100%",
        id: "__alloyId54"
    });
    $.__views.__alloyId44.add($.__views.__alloyId54);
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;