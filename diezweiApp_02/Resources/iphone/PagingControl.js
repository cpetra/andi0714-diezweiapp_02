var disabledBackground = "#aca8a9";

var disabledBorder = "#ffffff";

var enabledBackground = "#fae101";

var enabledBorder = "#ffffff";

var visitedBackground = "#d442a5";

PagingControl = function(scrollableView) {
    var container = Titanium.UI.createView({
        height: Ti.UI.SIZE,
        top: "20dp",
        width: Ti.UI.SIZE
    });
    var numberOfPages = scrollableView.getViews().length;
    var pages = [];
    var visitedPages = [];
    var previousPage = 0;
    for (var i = 0; numberOfPages > i; i++) {
        var page = Titanium.UI.createView({
            borderRadius: 7,
            borderWidth: "1.5dp",
            borderColor: disabledBorder,
            width: "14dp",
            height: "14dp",
            left: 22 * i + "dp",
            bottom: 0,
            backgroundColor: disabledBackground
        });
        pages.push(page);
        container.add(page);
    }
    pages[scrollableView.getCurrentPage()].setBackgroundColor(enabledBackground);
    pages[scrollableView.getCurrentPage()].setBorderColor(enabledBorder);
    visitedPages.push(scrollableView.getCurrentPage());
    checkValue = function(a, array) {
        Titanium.API.info(array);
        for (var i = 0; array.length > i; i++) if (a == array[i]) return true;
        return false;
    };
    onScrollEnd = function(event) {
        pages[previousPage].setBackgroundColor(disabledBackground);
        pages[previousPage].setBorderColor(disabledBorder);
        checkValue(previousPage, visitedPages) && pages[previousPage].setBackgroundColor(visitedBackground);
        pages[event.currentPage].setBackgroundColor(enabledBackground);
        pages[event.currentPage].setBorderColor(enabledBorder);
        previousPage = event.currentPage;
        visitedPages.push(event.currentPage);
    };
    scrollableView.addEventListener("scrollend", onScrollEnd);
    return container;
};

module.exports = PagingControl;