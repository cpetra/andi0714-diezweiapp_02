function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "home";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.home = Ti.UI.createView({
        layout: "vertical",
        width: "100%",
        backgroundColor: "#ffccdd",
        id: "home"
    });
    $.__views.home && $.addTopLevelView($.__views.home);
    $.__views.view_01 = Ti.UI.createView({
        backgroundColor: "#efb9d9",
        height: "13%",
        borderColor: "#a79aa4",
        id: "view_01"
    });
    $.__views.home.add($.__views.view_01);
    $.__views.title = Ti.UI.createLabel({
        left: "20dp",
        right: "20dp",
        height: Ti.UI.SIZE,
        color: "#000",
        font: {
            fontSize: "12dp"
        },
        textAlign: "center",
        touchEnabled: false,
        text: "Überprüfe alle punkte der Checkliste zum angegebenen Zeitpunkt, um mit einem guten Gefühl in deine Vorstellungsgespäche zu gehen.",
        id: "title"
    });
    $.__views.view_01.add($.__views.title);
    $.__views.__alloyId13 = Ti.UI.createView({
        width: "100%",
        height: "1dp",
        backgroundColor: "#000000",
        opacity: "0.3",
        id: "__alloyId13"
    });
    $.__views.home.add($.__views.__alloyId13);
    $.__views.view_02 = Ti.UI.createView({
        backgroundColor: "#ececec",
        height: "87%",
        layout: "vertical",
        id: "view_02"
    });
    $.__views.home.add($.__views.view_02);
    $.__views.__alloyId14 = Ti.UI.createView({
        height: "10dp",
        id: "__alloyId14"
    });
    $.__views.view_02.add($.__views.__alloyId14);
    $.__views.btn_01 = Alloy.createController("customButtonView", {
        id: "btn_01",
        __parentSymbol: $.__views.view_02
    });
    $.__views.btn_01.setParent($.__views.view_02);
    $.__views.__alloyId15 = Ti.UI.createView({
        height: "10dp",
        id: "__alloyId15"
    });
    $.__views.view_02.add($.__views.__alloyId15);
    $.__views.btn_02 = Alloy.createController("customButtonView", {
        id: "btn_02",
        __parentSymbol: $.__views.view_02
    });
    $.__views.btn_02.setParent($.__views.view_02);
    $.__views.__alloyId16 = Ti.UI.createView({
        height: "10dp",
        id: "__alloyId16"
    });
    $.__views.view_02.add($.__views.__alloyId16);
    $.__views.btn_03 = Alloy.createController("customButtonView", {
        id: "btn_03",
        __parentSymbol: $.__views.view_02
    });
    $.__views.btn_03.setParent($.__views.view_02);
    $.__views.__alloyId17 = Ti.UI.createView({
        height: "10dp",
        id: "__alloyId17"
    });
    $.__views.view_02.add($.__views.__alloyId17);
    $.__views.btn_04 = Alloy.createController("customButtonView", {
        id: "btn_04",
        __parentSymbol: $.__views.view_02
    });
    $.__views.btn_04.setParent($.__views.view_02);
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    $.btn_01.setData({
        title: "14 Tage vor jedem Termin",
        number: "+14"
    });
    $.btn_02.setData({
        title: "3 Tage vor jedem Termin",
        number: "+3"
    });
    $.btn_03.setData({
        title: "1 Tag vor jedem Termin",
        number: "+3"
    });
    $.btn_04.setData({
        title: "Am Tag deines Gespräches",
        number: "0"
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;