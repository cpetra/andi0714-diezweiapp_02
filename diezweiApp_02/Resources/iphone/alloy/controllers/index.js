function Controller() {
    function init() {
        pWidth = Ti.Platform.displayCaps.platformWidth;
        "android" == Ti.Platform.osname && (pWidth = Ti.Platform.displayCaps.platformWidth / Ti.Platform.displayCaps.logicalDensityFactor);
        Titanium.API.info("open page: " + openPage);
        Titanium.API.info("width: + " + pWidth);
        $.comp.left = "home" == openPage ? 0 : -pWidth;
        $.contDescriptionPage.left = pWidth;
        $.contDescriptionPage.width = pWidth;
        $.home.width = pWidth;
        $.comp.width = 2 * pWidth;
    }
    function handlerButtonEvent() {
        var b = Titanium.UI.createAnimation({
            left: -pWidth,
            duration: 500
        });
        $.comp.animate(b);
        openPage = "description";
        $.btnBack.visible = true;
    }
    function handlerButtonBack() {
        var b = Titanium.UI.createAnimation({
            left: 0,
            duration: 500
        });
        $.comp.animate(b);
        openPage = "home";
        $.btnBack.visible = false;
    }
    function handlerClickButtonDown() {
        alert("click");
    }
    function handlerOrientationChange() {
        init();
        Titanium.API.info("orientation changed");
    }
    function handlerCloseWindow() {
        Ti.App.removeEventListener("buttonClickedEvent", handlerButtonEvent);
        Ti.Gesture.removeEventListener("orientationchange", handlerOrientationChange);
        $.win1.removeEventListener("close", handlerCloseWindow);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.win1 = Ti.UI.createWindow({
        backgroundColor: "#dbdbdb",
        id: "win1",
        title: "Checklisten",
        modal: "true",
        navBarHidden: "true"
    });
    $.__views.win1 && $.addTopLevelView($.__views.win1);
    $.__views.__alloyId18 = Ti.UI.createView({
        layout: "vertical",
        width: "100%",
        top: "0",
        id: "__alloyId18"
    });
    $.__views.win1.add($.__views.__alloyId18);
    $.__views.__alloyId19 = Ti.UI.createView({
        width: "100%",
        height: "4dp",
        backgroundColor: "#4c4c4e",
        id: "__alloyId19"
    });
    $.__views.__alloyId18.add($.__views.__alloyId19);
    $.__views.__alloyId20 = Ti.UI.createView({
        width: "100%",
        height: "16dp",
        id: "__alloyId20"
    });
    $.__views.__alloyId18.add($.__views.__alloyId20);
    $.__views.linearGradient = Ti.UI.createView({
        backgroundGradient: {
            type: "linear",
            startPoint: {
                x: "0%",
                y: "50%"
            },
            endPoint: {
                x: "100%",
                y: "50%"
            },
            colors: [ {
                color: "white",
                offset: 0
            }, {
                color: "black",
                offset: .45
            }, {
                color: "white",
                offset: 1
            } ]
        },
        id: "linearGradient",
        width: "100%",
        height: "16dp"
    });
    $.__views.__alloyId20.add($.__views.linearGradient);
    $.__views.__alloyId21 = Ti.UI.createView({
        width: "100%",
        height: "2dp",
        backgroundColor: "#000000",
        id: "__alloyId21"
    });
    $.__views.__alloyId18.add($.__views.__alloyId21);
    $.__views.__alloyId22 = Ti.UI.createView({
        width: "100%",
        height: "1dp",
        backgroundColor: "#000000",
        opacity: "0.3",
        id: "__alloyId22"
    });
    $.__views.__alloyId18.add($.__views.__alloyId22);
    $.__views.__alloyId23 = Ti.UI.createView({
        width: "100%",
        height: "4dp",
        backgroundColor: "#fbdf00",
        id: "__alloyId23"
    });
    $.__views.__alloyId18.add($.__views.__alloyId23);
    $.__views.__alloyId24 = Ti.UI.createView({
        width: "100%",
        height: "1dp",
        backgroundColor: "#000000",
        opacity: "0.3",
        id: "__alloyId24"
    });
    $.__views.__alloyId18.add($.__views.__alloyId24);
    $.__views.__alloyId25 = Ti.UI.createView({
        width: "100%",
        height: "40dp",
        backgroundColor: "#d2d2d2",
        layout: "horizontal",
        id: "__alloyId25"
    });
    $.__views.__alloyId18.add($.__views.__alloyId25);
    $.__views.__alloyId26 = Ti.UI.createImageView({
        image: "/images/menu-up-icon.png",
        left: "10dp",
        id: "__alloyId26"
    });
    $.__views.__alloyId25.add($.__views.__alloyId26);
    $.__views.__alloyId27 = Ti.UI.createView({
        width: "1dp",
        height: "90%",
        backgroundColor: "#040203",
        opacity: "0.6",
        left: "10dp",
        top: "2dp",
        id: "__alloyId27"
    });
    $.__views.__alloyId25.add($.__views.__alloyId27);
    $.__views.title = Ti.UI.createLabel({
        left: "10dp",
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        font: {
            fontSize: "18dp",
            fontFamily: "Helvetica Neue",
            fontWeight: "bold"
        },
        color: "#000000",
        textAlign: "center",
        id: "title",
        text: "Checklisten"
    });
    $.__views.__alloyId25.add($.__views.title);
    $.__views.__alloyId28 = Ti.UI.createView({
        width: "100%",
        height: "1dp",
        backgroundColor: "#000000",
        opacity: "0.3",
        id: "__alloyId28"
    });
    $.__views.__alloyId18.add($.__views.__alloyId28);
    $.__views.__alloyId29 = Ti.UI.createScrollView({
        top: "68dp",
        contentWidth: "100%",
        id: "__alloyId29"
    });
    $.__views.win1.add($.__views.__alloyId29);
    $.__views.comp = Ti.UI.createView({
        id: "comp"
    });
    $.__views.__alloyId29.add($.__views.comp);
    $.__views.home = Ti.UI.createView({
        id: "home",
        left: "0"
    });
    $.__views.comp.add($.__views.home);
    $.__views.__alloyId30 = Alloy.createController("home", {
        id: "__alloyId30",
        __parentSymbol: $.__views.home
    });
    $.__views.__alloyId30.setParent($.__views.home);
    $.__views.contDescriptionPage = Ti.UI.createView({
        id: "contDescriptionPage"
    });
    $.__views.comp.add($.__views.contDescriptionPage);
    $.__views.__alloyId31 = Alloy.createController("descriptionPage", {
        id: "__alloyId31",
        __parentSymbol: $.__views.contDescriptionPage
    });
    $.__views.__alloyId31.setParent($.__views.contDescriptionPage);
    $.__views.__alloyId32 = Ti.UI.createView({
        width: "100%",
        height: "4dp",
        bottom: "30dp",
        backgroundColor: "#fbdf00",
        id: "__alloyId32"
    });
    $.__views.win1.add($.__views.__alloyId32);
    $.__views.__alloyId33 = Ti.UI.createView({
        bottom: "0",
        height: "30dp",
        width: "100%",
        backgroundColor: "#484649",
        layout: "horizontal",
        id: "__alloyId33"
    });
    $.__views.win1.add($.__views.__alloyId33);
    $.__views.btnBack = Ti.UI.createView({
        id: "btnBack",
        width: "40dp",
        visible: "false"
    });
    $.__views.__alloyId33.add($.__views.btnBack);
    $.__views.__alloyId34 = Ti.UI.createImageView({
        image: "/images/btn-back.png",
        id: "__alloyId34"
    });
    $.__views.btnBack.add($.__views.__alloyId34);
    $.__views.__alloyId35 = Ti.UI.createView({
        opacity: "0.4",
        height: "80%",
        width: "1dp",
        backgroundColor: "#fbdf00",
        id: "__alloyId35"
    });
    $.__views.__alloyId33.add($.__views.__alloyId35);
    $.__views.btnDown_01 = Ti.UI.createView({
        width: "25%",
        height: "100%",
        id: "btnDown_01"
    });
    $.__views.__alloyId33.add($.__views.btnDown_01);
    $.__views.__alloyId36 = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        font: {
            fontSize: "10dp",
            fontFamily: "Helvetica Neue",
            fontWeight: "bold"
        },
        color: "#fbdf00",
        textAlign: "center",
        text: "HOME",
        id: "__alloyId36"
    });
    $.__views.btnDown_01.add($.__views.__alloyId36);
    $.__views.__alloyId37 = Ti.UI.createView({
        opacity: "0.4",
        height: "80%",
        width: "1dp",
        backgroundColor: "#fbdf00",
        id: "__alloyId37"
    });
    $.__views.__alloyId33.add($.__views.__alloyId37);
    $.__views.__alloyId38 = Ti.UI.createView({
        width: "25%",
        id: "__alloyId38"
    });
    $.__views.__alloyId33.add($.__views.__alloyId38);
    $.__views.btnDown_02 = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        font: {
            fontSize: "10dp",
            fontFamily: "Helvetica Neue",
            fontWeight: "bold"
        },
        color: "#fbdf00",
        textAlign: "center",
        text: "Info",
        id: "btnDown_02"
    });
    $.__views.__alloyId38.add($.__views.btnDown_02);
    $.__views.__alloyId39 = Ti.UI.createView({
        opacity: "0.4",
        height: "80%",
        width: "1dp",
        backgroundColor: "#fbdf00",
        id: "__alloyId39"
    });
    $.__views.__alloyId33.add($.__views.__alloyId39);
    $.__views.__alloyId40 = Ti.UI.createView({
        width: "25%",
        id: "__alloyId40"
    });
    $.__views.__alloyId33.add($.__views.__alloyId40);
    $.__views.btnDown_03 = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        font: {
            fontSize: "10dp",
            fontFamily: "Helvetica Neue",
            fontWeight: "bold"
        },
        color: "#fbdf00",
        textAlign: "center",
        text: "Impressum",
        id: "btnDown_03"
    });
    $.__views.__alloyId40.add($.__views.btnDown_03);
    $.__views.__alloyId41 = Ti.UI.createView({
        opacity: "0.4",
        height: "80%",
        width: "1dp",
        backgroundColor: "#fbdf00",
        id: "__alloyId41"
    });
    $.__views.__alloyId33.add($.__views.__alloyId41);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var pWidth = 320;
    var openPage = "home";
    $.win1.orientationModes = [ Ti.UI.LANDSCAPE_LEFT, Ti.UI.LANDSCAPE_RIGHT, Ti.UI.PORTRAIT ];
    init();
    $.win1.open();
    Ti.App.addEventListener("buttonClickedEvent", handlerButtonEvent);
    $.btnBack.addEventListener("click", handlerButtonBack);
    $.btnDown_01.addEventListener("click", handlerClickButtonDown);
    $.btnDown_02.addEventListener("click", handlerClickButtonDown);
    $.btnDown_03.addEventListener("click", handlerClickButtonDown);
    Ti.Gesture.addEventListener("orientationchange", handlerOrientationChange);
    $.win1.addEventListener("close", handlerCloseWindow);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;