function Controller() {
    function handlerClickCheckbox() {
        $.imgCheck.visible = $.imgCheck.visible ? false : true;
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "descriptionPage";
    var __parentSymbol = arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.descriptionPage = Ti.UI.createView({
        width: "100%",
        layout: "vertical",
        id: "descriptionPage"
    });
    $.__views.descriptionPage && $.addTopLevelView($.__views.descriptionPage);
    $.__views.__alloyId2 = Ti.UI.createView({
        height: "10dp",
        id: "__alloyId2"
    });
    $.__views.descriptionPage.add($.__views.__alloyId2);
    $.__views.btn = Alloy.createController("customButtonView", {
        id: "btn",
        __parentSymbol: $.__views.descriptionPage
    });
    $.__views.btn.setParent($.__views.descriptionPage);
    $.__views.view_01 = Ti.UI.createView({
        backgroundColor: "#ffffff",
        borderColor: "#cccccc",
        borderWidth: "1.5dp",
        left: "10dp",
        right: "10dp",
        height: "50dp",
        borderRadius: "5",
        top: "10dp",
        id: "view_01"
    });
    $.__views.descriptionPage.add($.__views.view_01);
    $.__views.contCheck = Ti.UI.createView({
        backgroundGradient: {
            type: "linear",
            startPoint: {
                x: "0%",
                y: "0%"
            },
            endPoint: {
                x: "0%",
                y: "100%"
            },
            colors: [ {
                color: "#eb8dd7",
                offset: 0
            }, {
                color: "#f9aae3",
                offset: .45
            }, {
                color: "#eb8dd7",
                offset: 1
            } ]
        },
        left: "3dp",
        top: "3dp",
        right: "3dp",
        bottom: "3dp",
        id: "contCheck"
    });
    $.__views.view_01.add($.__views.contCheck);
    $.__views.txtCheck = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        font: {
            fontSize: "12dp",
            fontFamily: "Helvetica Neue",
            fontWeight: "bold"
        },
        color: "#000000",
        left: "10dp",
        text: "Ich habe mir eigene Fragen überlegt.",
        id: "txtCheck"
    });
    $.__views.contCheck.add($.__views.txtCheck);
    $.__views.cb = Ti.UI.createView({
        width: "25dp",
        height: "25dp",
        borderColor: "#000000",
        borderWidth: "1.5dp",
        borderRadius: "5",
        right: "10dp",
        id: "cb"
    });
    $.__views.contCheck.add($.__views.cb);
    $.__views.__alloyId3 = Ti.UI.createImageView({
        image: "/images/icon-uncheck.png",
        id: "__alloyId3"
    });
    $.__views.cb.add($.__views.__alloyId3);
    $.__views.imgCheck = Ti.UI.createImageView({
        id: "imgCheck",
        image: "/images/icon-check.png"
    });
    $.__views.cb.add($.__views.imgCheck);
    $.__views.containerScroller = Ti.UI.createView({
        height: Ti.UI.SIZE,
        left: "10dp",
        right: "10dp",
        top: "10dp",
        id: "containerScroller"
    });
    $.__views.descriptionPage.add($.__views.containerScroller);
    var __alloyId4 = [];
    $.__views.__alloyId5 = Alloy.createController("tippView", {
        id: "__alloyId5",
        __parentSymbol: __parentSymbol
    });
    __alloyId4.push($.__views.__alloyId5.getViewEx({
        recurse: true
    }));
    $.__views.__alloyId6 = Alloy.createController("tippView", {
        id: "__alloyId6",
        __parentSymbol: __parentSymbol
    });
    __alloyId4.push($.__views.__alloyId6.getViewEx({
        recurse: true
    }));
    $.__views.__alloyId7 = Alloy.createController("tippView", {
        id: "__alloyId7",
        __parentSymbol: __parentSymbol
    });
    __alloyId4.push($.__views.__alloyId7.getViewEx({
        recurse: true
    }));
    $.__views.__alloyId8 = Alloy.createController("tippView", {
        id: "__alloyId8",
        __parentSymbol: __parentSymbol
    });
    __alloyId4.push($.__views.__alloyId8.getViewEx({
        recurse: true
    }));
    $.__views.__alloyId9 = Alloy.createController("tippView", {
        id: "__alloyId9",
        __parentSymbol: __parentSymbol
    });
    __alloyId4.push($.__views.__alloyId9.getViewEx({
        recurse: true
    }));
    $.__views.__alloyId10 = Alloy.createController("tippView", {
        id: "__alloyId10",
        __parentSymbol: __parentSymbol
    });
    __alloyId4.push($.__views.__alloyId10.getViewEx({
        recurse: true
    }));
    $.__views.__alloyId11 = Alloy.createController("tippView", {
        id: "__alloyId11",
        __parentSymbol: __parentSymbol
    });
    __alloyId4.push($.__views.__alloyId11.getViewEx({
        recurse: true
    }));
    $.__views.__alloyId12 = Alloy.createController("tippView", {
        id: "__alloyId12",
        __parentSymbol: __parentSymbol
    });
    __alloyId4.push($.__views.__alloyId12.getViewEx({
        recurse: true
    }));
    $.__views.sview_slider = Ti.UI.createScrollableView({
        views: __alloyId4,
        id: "sview_slider",
        showPagingControl: "false",
        height: Ti.UI.SIZE
    });
    $.__views.containerScroller.add($.__views.sview_slider);
    $.__views.viewPaging = Ti.UI.createView({
        id: "viewPaging",
        height: Ti.UI.SIZE
    });
    $.__views.descriptionPage.add($.__views.viewPaging);
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    $.btn.changeVisibilityArrow(false);
    $.cb.addEventListener("click", handlerClickCheckbox);
    require("PagingControl");
    var sViewPagingControl = new PagingControl($.sview_slider);
    $.viewPaging.add(sViewPagingControl);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;